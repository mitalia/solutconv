# README #

Code written for my BSc thesis in Physics.

You may find useful the Python wrappers for:

- National Instruments' NI435x datalogger
- National Instruments' IMAQ low-ish-level interface (just enough to acquire some frames)
- HP/Agilent 34401A multimeter (via PyVISA) (should work with other multimeters)
- Kepco ABC 15-70M power supply (via PyVISA) (should work with other power supplies)