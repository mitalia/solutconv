from ctypes import *
import numpy as np

ni=windll.imaq

# Types
INTERFACE_ID = c_int
SESSION_ID = c_int

# Constants
# Attributes
_IMG_BASE = 0x3FF60000
IMG_ATTR_ROI_WIDTH = (_IMG_BASE + 0x01A6)
IMG_ATTR_ROI_HEIGHT = (_IMG_BASE + 0x01A7)
IMG_ATTR_BITSPERPIXEL = (_IMG_BASE + 0x0066)
IMG_ATTR_BYTESPERPIXEL = (_IMG_BASE + 0x0067)

class ImgInterface:
    def __init__(self, interfaceName):
        self.interfaceID=None
        self.sessionID=None

        self.interfaceID = INTERFACE_ID(0)
        ni.imgInterfaceOpen(c_char_p(interfaceName), POINTER(INTERFACE_ID)(self.interfaceID))

    def sessionOpen(self):
        self.sessionID=SESSION_ID(0)
        return ni.imgSessionOpen(self.interfaceID, POINTER(SESSION_ID)(self.sessionID))


    def sessionClose(self):
        if self.sessionID:
            status=ni.imgClose(self.sessionID, c_int(1))
            self.sessionID=None
            return status

    def interfaceClose(self):
        if self.interfaceID:
            status=ni.imgClose(self.interfaceID, c_int(1))
            self.interfaceID=None
            return status


    def getSessionAttribute(self, attribute):
        ret = c_int(0)
        ni.imgGetAttribute(self.sessionID, c_int(attribute), POINTER(c_int)(ret))
        return ret.value

    def getInterfaceAttribute(self, attribute):
        ret = c_int(0)
        ni.imgGetAttribute(self.interfaceID, c_int(attribute), POINTER(c_int)(ret))
        return ret.value

    def width(self):
        return self.getSessionAttribute(IMG_ATTR_ROI_WIDTH)

    def height(self):
        return self.getSessionAttribute(IMG_ATTR_ROI_HEIGHT)

    def bitsPerPixel(self):
        return self.getSessionAttribute(IMG_ATTR_BITSPERPIXEL)

    def getBufferSize(self):
        ret = c_int(0)
        ni.imgSessionGetBufferSize(self.sessionID, POINTER(c_int)(ret))
        return ret.value

    def snap(self):
        if self.bitsPerPixel() != 8:
            raise NotImplementedError("Currently only 8 bpp images are supported")
        ret = np.zeros((self.height(), self.width()), dtype=np.uint8)
        t = ret.ctypes.data_as(POINTER(c_byte))
        ptr=POINTER(POINTER(c_byte))(t)
        ni.imgSnap(self.sessionID, ptr)
        return ret

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.sessionClose()
        self.interfaceClose()

if __name__=="__main__":
    with ImgInterface("img0") as i:
        import PIL.Image
        i.sessionOpen()
        print i.width(), i.height(), i.bitsPerPixel()
        arr=i.snap()
        print arr
        img=PIL.Image.fromarray(arr)
        img.save("test.png")

