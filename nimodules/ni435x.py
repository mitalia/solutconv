from ctypes import *

from pyvisa.vpp43_types import *
from pyvisa.vpp43_constants import *


ni=windll.ni435x_32

class ni435x:

    # Channel modes
    VOLTAGE = 0  # Voltage
    THERM_B = 1  # Thermocouple B
    THERM_E = 2  # Thermocouple E
    THERM_J = 3  # Thermocouple J
    THERM_K = 4  # Thermocouple N
    THERM_N = 5  # Thermocouple K
    THERM_R = 6  # Thermocouple R
    THERM_S = 7  # Thermocouple S
    THERM_T = 8  # Thermocouple T
    RTD_ITS_90 = 9  # RTD ITS-90
    TRD_AMERICAN = 10  # RTD American
    RTD_DIN_43760 = 11  # RTD DIN 43760
    RESISTANCE = 12  # Resistance
    THERM_CUSTOM = 13  # Thermocouple Custom

    # Powerline frequency
    FREQ_50HZ = 0  #     50 Hz
    FREQ_60HZ = 1  #     60 Hz

    # Reading rate
    FAST = 0
    SLOW = 1

    # Acquisition start/stop constants
    START = 0  # START
    STOP = 1  # STOP

    def __init__(self, resourceName='DAQ::1', IDQuery=1, resetDevice=0):
        self.DAQsession=ViSession()
        self.Init(resourceName, IDQuery, resetDevice)

    def __del__(self):
        self.Close()

    def Init(self, resourceName, IDQuery, resetDevice):
        resourceName=ViRsrc(resourceName)
        IDQuery=ViBoolean(IDQuery)
        resetDevice=ViBoolean(resetDevice)
        return ni.NI435X_init(resourceName, IDQuery, resetDevice, ViPSession(self.DAQsession))

    def Set_Scan_List(self, channelString):
        channelString=ViString(channelString)
        return ni.NI435X_Set_Scan_List(self.DAQsession, channelString)

    def Set_Channel_Mode(self, channelString, channelMode):
        channelString=ViString(channelString)
        channelMode=ViInt16(channelMode)
        return ni.NI435X_Set_Channel_Mode(self.DAQsession, channelString, channelMode)

    def Set_Auto_Zero(self, autoZero):
        autoZero=ViBoolean(autoZero)
        return ni.NI435X_Set_Auto_Zero(self.DAQsession, autoZero)

    def Set_Gnd_Ref(self, channelString, groundReference):
        channelString=ViString(channelString)
        groundReference=ViInt16(groundReference)
        return ni.NI435X_Set_Gnd_Ref(self.DAQsession, channelString, groundReference)

    def Set_Reading_Rate(self, readingRate):
        readingRate=ViInt16(readingRate)
        return ni.NI435X_Set_Reading_Rate(self.DAQsession, readingRate)


# Bacata
#    def Set_Range(self, channelString, lowLimits, highLimits):
#        chans=len(channelString.split(','))
#        lowLimits_ = (ViReal64*chans)()
#        highLimits_ = (ViReal64*chans)()
#        for i in range(chans):
#            lowLimits_[i]=lowLimits[i]
#            highLimits_[i] = highLimits[i]
#        channelString=ViString(channelString)
#        return ni.NI435X_Set_Range(self.DAQsession, channelString, byref(lowLimits_), byref(highLimits_))

    def Set_Powerline_Frequency(self, powerlineFrequency):
        powerlineFrequency=ViInt16(powerlineFrequency)
        return ni.NI435X_Set_Powerline_Frequency(self.DAQsession, powerlineFrequency)

    def Set_Number_Of_Scans(self, numberOfScans):
        numberOfScans=ViInt16(numberOfScans)
        return ni.NI435X_Set_Number_Of_Scans(self.DAQsession, numberOfScans)

    def Acquisition_StartStop(self, control):
        control=ViInt16(control)
        return ni.NI435X_Acquisition_StartStop(self.DAQsession, control)

    def Get_Number_Of_Channels(self):
        num=ViInt16(0)
        status=ni.NI435X_Get_Number_Of_Channels(self.DAQsession, POINTER(ViInt16)(num))
        return int(num.value)

    def Read(self, numberOfScans, timeout):
        scansRead=(ViReal64*(numberOfScans*self.Get_Number_Of_Channels()))()
        numberOfScans=ViInt32(numberOfScans)
        timeout=ViReal64(timeout)
        status = ni.NI435X_Read(self.DAQsession, numberOfScans, timeout, byref(scansRead))
        if status!=0:
            return None
        return list(scansRead)

    def Close(self):
        status=ni.NI435X_close(self.DAQsession)
        self.DAQsession=0
        return status

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.Close()
