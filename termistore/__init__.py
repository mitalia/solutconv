import numpy as np
from scipy.optimize import curve_fit
import csv
import math

""" Loads the Xs and Ys of the given TSV table of thermistor characteristics;
the expected format is "temperature \t k", where temperature is in
celsius and k is the coefficient to apply to R25 to obtain the actual
resistance.

Returns the (xs, ys) tuple """
def load_XY(filename='thermistor_data.tsv', R25=100000):
    xs=[]
    ys=[]
    with open(filename, 'r') as tsvfile:
        datareader=csv.reader(tsvfile, delimiter='\t')
        for row in datareader:
            if len(row)!=0:
                ys.append(float(row[0])+273.15)
                xs.append(float(row[1])*R25)
    return (xs, ys)

""" Steinhart-Hart equation; returns the temperature (in Kelvin) corresponding
to the given resistance"""
def steinhart_hart(R, a, b, c):
    l=np.log(R)
    return 1./(a+b*l+c*l*l*l)

""" Performs the fit of the Steinhart-Hart equation over the given xs and ys,
and returns the resistance -> temperature (in Kelvin) lambda function """
def build_conversion_function(xs, ys):
    params=curve_fit(steinhart_hart, xs, ys, [1.0e-3, 1.0e-4, 1.0e-8])
    return lambda R, p=params[0]: steinhart_hart(R, *p)

if __name__=='__main__':
    # Module test
    l=build_conversion_function(*load_XY())
    m=lambda R: l(R)-273.15
    print m(87351)
