# -*- coding: utf-8 -*-
import os
import os.path
import threading
import termistore
import pidcontroller
import Queue
from visainstruments.multimeter import *
from visainstruments.powersupply import *
from nimodules.ni435x import *
from nimodules.imaq import *
from PIL import Image
import numpy as np
from datetime import datetime
import time
import itertools
import glob

NoImageAcquisition=False

class TempMgr:
    # 0: sopra (canale 3)
    # 1: sotto (canale 2)

    def __init__(self, Controllers, MultimetersAddresses, SuppliesAddresses, ThermFunction):
        self.tMax=70
        self.tMin=5
        self.notifyFunc=None
        self.disableOutput=False

        self.controllers=Controllers
        self.mms=map(Multimeter, MultimetersAddresses)
        for mm in self.mms:
            mm.setMode(mm.modeResistance)
            if mm.read()>115000:
                mm.setMode(mm.modeResistance, '200000,3')
                mm.coarseMode=True
            else:
                mm.setMode(mm.modeResistance,'100000,1')
                mm.coarseMode=False

        self.supplies=[]
        for addr in SuppliesAddresses:
            ps=PowerSupply(addr)
            ps.setVolt(14)
            ps.setCurr(0)
            ps.setOutput(False)
            self.supplies.append(ps)

        self.thermFunction=ThermFunction

    def run(self):
        self.quit=False
        seqFaultCount=0
        while not self.quit:
            ress=[mm.read() for mm in self.mms]
            temps=[]
            currs=[]
            for i in range(0, 2):
                if ress[i]>120000 and not self.mms[i].coarseMode:
                    self.mms[i].setMode(mm.modeResistance, '200000,3')
                    self.mms[i].coarseMode=True
                elif ress[i]<110000 and self.mms[i].coarseMode:
                    self.mms[i].setMode(mm.modeResistance,  '100000,1')
                    self.mms[i].coarseMode=False
                temp=self.thermFunction(ress[i])-273.15
                if temp>self.tMax or temp<self.tMin:
                    seqFaultCount+=1
                    if temp<-100 and seqFaultCount<5:
                        temp=self.controllers[i].target
                        print "Sensor fault", i, temp, seqFaultCount
                    else:
                        self.disableOutput=True
                        print "Disabling output:", i, temp
                else:
                    seqFaultCount=0
                self.controllers[i].addMeasure(temp)
                corr=self.controllers[i].getCorrection()
                self.supplies[i].setOutput(not self.disableOutput)
                if not self.disableOutput:
                    self.supplies[i].setCurr(min(max(corr, 0), 6))
                temps.append(temp)
                currs.append(corr)
            if self.notifyFunc is not None:
                self.notifyFunc(temps=temps, currs=currs, outputOn=not self.disableOutput)

            for i in range(0,2):
                self.supplies[i].setOutput(False)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        for ps in self.supplies:
            ps.reset()

class WatchDog:

    def __init__(self, TM, NIIface, NIThermFunction):
        self.sleepBetweenChecks=5
        self.panicNotifyCoolOffPeriod=1200
        self.lastPanicTime=datetime(1970, 1, 1)
        self.extTemps=[]
        self.tMax=70
        self.tMin=5

        self.tempMgr=TM
        self.niIface=NIIface
        self.niThermFunction=NIThermFunction

    def run(self):
        self.quit=False
        while not self.quit:
            temps=[]
            temps.extend(self.extTemps)
            self.niIface.Acquisition_StartStop(self.niIface.START)
            temps.extend([self.niThermFunction(t)-273.15 for t in self.niIface.Read(1, 5)])
            self.niIface.Acquisition_StartStop(self.niIface.STOP)
            for t in temps:
                if (t>self.tMax or t<self.tMin):
                    self.panicAction(temps)
                    break
            print "\n[WatchDog]", temps
            time.sleep(self.sleepBetweenChecks)

    def panicAction(self, temps):
        self.tempMgr.disableOutput=True
        if (datetime.now()-self.lastPanicTime).total_seconds()>=self.panicNotifyCoolOffPeriod:
            self.panicNotify(temps)
        self.lastPanicTime=datetime.now()

    def panicNotify(self, temps):
        print "\n[WatchDot]","PANIC: temperatures out of range", temps

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.niIface.Close()

class CameraMgr:

    def __init__(self, InterfaceAddress):
        self.notifyFunc=None
        self.data=None

        self.imgInterface=ImgInterface(InterfaceAddress)
        self.imgInterface.sessionOpen()

    def run(self):
        self.quit=False
        while not self.quit:
            img=self.imgInterface.snap()
            if self.notifyFunc is not None:
                self.notifyFunc(img=img, data=self.data)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.imgInterface.sessionClose()
        self.imgInterface.interfaceClose()


class Main:
    outPath=None
    bg=None
    wt=None
    ni=None
    watchDog=None
    tc=None
    tt=None
    cm=None
    ct=None

    def __init__(self):
        pass

    def SaveImg(self, img, data):
        if not NoImageAcquisition:
            if data is None:
                return
            image, temp = Image.fromarray((img-self.bg+127).astype(np.uint8)), data['temps']
            #print "Temperatures: ", temp, " C"
            outFile=self.outPath+"/%s - %f-%f C.png" % (datetime.now().strftime("%Y-%m-%d %H.%M.%S"), temp[0], temp[1])
            #print "Saving to", outFile
            image.save(outFile)
        time.sleep(5)

    def StoreTemps(self, temps, currs, outputOn):
        self.cm.data={'temps': temps}
        self.watchDog.extTemps=temps
        i=0
        print '\r',
        print "ON" if outputOn else "OFF", "-",
        for temp, curr in zip(temps, currs):
            print "%d: %0.4f C %0.2f A" % (i, temp, curr), "(%0.4f+-%0.4f)" % self.conts[i].getMeanStdDev(),
            i+=1
        #print

    def StartAcquisition(self):
        #OtherBG="d:/matteo-data/pictures/2013-10-24/1 - 10 mm - 9 C/00000 - bg.png"
        OtherBG=None
        if not NoImageAcquisition:
            if OtherBG:
                self.bg=np.asarray(Image.open(OtherBG))
            else:
                self.bg=self.cm.imgInterface.snap()
            Image.fromarray(self.bg).save(self.outPath+'/00000 - bg.png')
            self.bg=self.bg.astype(np.int16)
        self.wt.start()
        self.ct.start()
        self.tt.start()

    def run(self):
        self.basePath = os.path.abspath('../pictures')
        if os.path.exists(self.basePath) and not os.path.isdir(self.basePath):
            raise Exception('\''+ self.basePath + '\' exists and is not a directory')
        for i in itertools.count(1):
            self.outPath=self.basePath+"/%s/%d"%(datetime.now().strftime('%Y-%m-%d') , i)
            if not (os.path.exists(self.outPath) or len(glob.glob(self.outPath+"[!0-9]*"))):
                break

        print 'Output path base:', self.outPath
        print 'Extra text:',
        extra=raw_input()
        if extra.strip()!='':
            self.outPath+=' - ' + extra.strip()
        print 'Output path:', self.outPath
        os.makedirs(self.outPath)
        
        with open(self.outPath+'/desc.info', 'w') as out:
            out.write(extra)

        self.conts=[]
        for i in range(2):
            tcont=pidcontroller.PIDController(200)
            tcont.timeStep=0.20
            tcont.p=10.0
            tcont.i=0.1
            tcont.d=0.2
            tcont.setTarget(24.)
            self.conts.append(tcont)
        self.conts[0].setTarget(35.)
        self.conts[0].invertParams()
        l=termistore.build_conversion_function(*termistore.load_XY('termistore/thermistor_data.tsv', 100000))
        self.tm=TempMgr(self.conts, ['GPIB::22', 'GPIB::23'], ['GPIB::12', 'GPIB::17'], l)

        self.ni=ni435x()
        self.ni.Set_Scan_List('2,3')
        self.ni.Set_Channel_Mode('2,3', self.ni.RESISTANCE)
        self.ni.Set_Auto_Zero(False)
        self.ni.Set_Powerline_Frequency(self.ni.FREQ_50HZ)
        self.ni.Set_Reading_Rate(self.ni.FAST)
        self.watchDog=WatchDog(self.tm, self.ni, termistore.build_conversion_function(*termistore.load_XY('termistore/thermistor_data.tsv', 10000)))

        self.bg=None
        self.cm=CameraMgr('img0')

        self.cm.notifyFunc=self.SaveImg
        self.tm.notifyFunc=self.StoreTemps

        self.ct=threading.Thread(target=self.cm.run)
        self.wt=threading.Thread(target=self.watchDog.run)
        self.tt=threading.Thread(target=self.tm.run)

        print 'Program ready; use: '
        print '- `start` to start the acquisition right now'
        print '- `auto delay [length]` to do an automated acquisition'
        print '- `exit` to exit the application'
        print 'Other commands available, check the source'
        while True:
            cmd=raw_input().strip()
            tokens=[t for t in cmd.split(' ') if t.strip()!='']
            #print tokens
            if len(tokens)==0:
                continue
            try:
                if tokens[0]=='exit' or tokens[0]=='quit':
                    break
                elif tokens[0]=='start':
                    self.StartAcquisition()
                elif tokens[0]=='auto':
                    time.sleep(float(tokens[1]))
                    self.StartAcquisition()
                    if len(tokens)>=3:
                        time.sleep(float(tokens[2]))
                        break
                elif tokens[0]=='settarget':
                    t1=float(tokens[1])
                    t2=float(tokens[2])
                    self.tm.controllers[0].setTarget(t1)
                    self.tm.controllers[1].setTarget(t2)
                elif tokens[0]=='showtarget':
                    print self.tm.controllers[0].target, self.tm.controllers[1].target
                elif tokens[0]=='setp':
                    p=float(tokens[1])
                    self.conts[0].p=-p
                    self.conts[1].p=p
                elif tokens[0]=='seti':
                    i=float(tokens[1])
                    self.conts[0].i=-i
                    self.conts[1].i=i
                elif tokens[0]=='setd':
                    d=float(tokens[1])
                    self.conts[0].d=-d
                    self.conts[1].d=d
                elif tokens[0]=='showparams':
                    for i, c in enumerate(self.conts):
                        print "%d:"%i, "p:", c.p, "i:", c.i, "d:", c.d, "target:", c.target
                elif tokens[0]=='disableoutput':
                    self.tm.disableOutput=True
                elif tokens[0]=='enableoutput':
                    self.tm.disableOutput=False
                else:
                    print "Unrecognized command"
            except Exception as ex:
                print ex
        
        print "Quitting..."
        self.tm.quit=self.cm.quit=True
        if self.tt.isAlive():
            self.tt.join()
        if self.ct.isAlive():
            self.ct.join()
        self.watchDog.quit=True
        if self.wt.isAlive():
            self.wt.join()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.cm.__exit__(type, value, traceback)
        self.watchDog.__exit__(type, value, traceback)
        self.tm.__exit__(type, value, traceback)

if __name__=='__main__':
    with Main() as m:
        m.run()
