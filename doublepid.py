# -*- coding: utf-8 -*-
import termistore
import pidcontroller
from visainstruments.powersupply import *
from nimodules.ni435x import *
pd=ni435x()
pd.Set_Scan_List('3,2')
pd.Set_Channel_Mode('3,2', ni435x.RESISTANCE)

# 0: sopra (canale 3)
# 1: sotto (canale 2)

conts=[]
for i in range(0, 2):
    cont=pidcontroller.PIDController(20)
    cont.timeStep=0.5
    cont.p=1.8
    cont.i=0.025
    cont.setTarget(22)
    conts.append(cont)

pss=[]
pss.append(PowerSupply('GPIB::12'))
pss.append(PowerSupply('GPIB::17'))
for ps in pss:
    ps.setVolt(12)
    ps.setCurr(0)
    ps.setOutput(True)

l=termistore.build_conversion_function(*termistore.load_XY('termistore/dati termistore.tsv'))
pd.Acquisition_StartStop(ni435x.START)
while True:
    pd.Set_Number_Of_Scans(1)
    ress=pd.Read(2, -1)
    for i in range(0, 2):
        temp=l(ress[i])-273.15
        conts[i].addMeasure(temp)
        corr=conts[i].getCorrection()
        pss[i].setCurr(min(max(corr, 0), 5))
        print "%d: %f C - %f A" % (i, temp, corr)
pd.Acquisition_StartStop(ni435x.STOP)
pd.Close()
