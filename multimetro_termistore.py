# -*- coding: utf-8 -*-
import termistore
import pidcontroller
from visainstruments.multimeter import *
from visainstruments.powersupply import *
from nimodules.ni435x import ni435x

multiaddresses=['GPIB::22', 'GPIB::23']
multis=[Multimeter(addr) for addr in multiaddresses]
if True:
#with ni435x() as ni:
    #ni.Set_Scan_List('2,3')
    #ni.Set_Channel_Mode('2,3', #ni.RESISTANCE)
    #ni.Set_Auto_Zero(False)
    #ni.Set_Powerline_Frequency(#ni.FREQ_50HZ)
    #ni.Set_Reading_Rate(#ni.FAST)

    for multi in multis:
        multi.setMode(multi.modeResistance,'200000,3')
        multi.displayClear()
        print multi.mm.ask('RES:RES?')

    l=termistore.build_conversion_function(*termistore.load_XY('termistore/thermistor_data.tsv', 100000))
    m=termistore.build_conversion_function(*termistore.load_XY('termistore/thermistor_data.tsv', 10000))
    started=False
    try:
        while True:
            #ni.Acquisition_StartStop(#ni.START)
            started=True
            #for res in #ni.Read(1, -1):
            if False:
                temp=m(res)-273.15
                s="%f C" % temp
                print "%f Ohm - " % res, s,
            #ni.Acquisition_StartStop(#ni.STOP)
            started=False
            print
            for multi in multis:
                res=multi.read()
                temp=l(res)-273.15
                s="%f C" % temp
                multi.displayText(s)
                print "%f Ohm - " % res, s,
            print
    finally:
        if started:
            pass
            #ni.Acquisition_StartStop(#ni.STOP)
