from PIL import Image
import numpy as np
import sys
from os import listdir
from os.path import isfile, isdir, join
import pickle
import datetime
import re

rootDir=u'd:/matteo-data/pictures'
runMaxDeltaT=0.5
runMinLength=100
mainRunMinMeanDeltaT=1.0
summaries=[]
rebuildSummaries=False
skipFullStats=False

def recurseSearch(curDir):
    print "In",curDir
    for d in listdir(curDir):
        absD=join(curDir,d)
        if not isdir(absD):
            continue
        if isfile(join(absD,u'00000 - bg.png')):
            processDir(absD)
        else:
            recurseSearch(absD)

def processDir(dataDir):
    global summaries
    localSFS=skipFullStats
    print "Processing", dataDir
    statsFile=join(dataDir, u'fullstats.tsv')
    if isfile(statsFile) and not rebuildSummaries:
        localSFS=True
    if localSFS:
        statsFile='NUL'
    runsFile=join(dataDir, u'runs.tsv')
    summaryFile=join(dataDir, u'summary.pickle')
    if not rebuildSummaries and isfile(summaryFile):
        print "summaryFile already present, recycling..."
        with open(summaryFile, 'rb') as summaryFD:
            try:
                summaries.append(pickle.load(summaryFD))
                return
            except Exception as ex:
                print "Exception in loading summary", repr(ex)
                print "Recreating it..."

    firstDate=None # =(
    deltaTRun=[]
    runsData=[]
    date=None

    def saveRun(run, rd):
        mean=np.mean(run)
        runData={'length': len(run), 'mean': np.mean(run), 'stddev': np.std(run)}
        rd.append(runData)
        print 'New run:', runData
        runsFD.write("%d\t%f\t%f\n" % (runData['length'], runData['mean'], runData['stddev']))


    with open(statsFile, 'w') as statsFD, open(runsFile, 'w') as runsFD, open(summaryFile, 'wb') as summaryFD:
        runsFD.write("Length\tMean\tStdDev\n")
        for f in sorted(listdir(dataDir)):
            absF=join(dataDir, f)
            if not (isfile(absF) and f.lower().endswith('.png')) or 'bg' in f:
                continue
            if not localSFS:
                arr=np.asarray(Image.open(absF)).astype(np.int16)-127
                variance=np.mean(np.square(arr))
            date=datetime.datetime.strptime(' '.join(f.split(' ')[0:2]),"%Y-%m-%d %H.%M.%S")
            if firstDate is None:
                firstDate=date
            time = (date-firstDate).total_seconds()
            a, u=tuple([float(t) for t in f.split(' ')[3].split('-') if t!=''])
            deltaT=a-u
            if len(deltaTRun)==0:
                deltaTRun=[deltaT]
            else:
                if abs(deltaTRun[0]-deltaT)<runMaxDeltaT:
                    deltaTRun.append(deltaT)
                else:
                    if len(deltaTRun)>=runMinLength:
                        saveRun(deltaTRun, runsData)
                    deltaTRun=[deltaT]
            if not localSFS:
                statsFD.write("%f\t%f\t%f\n" % (time, variance, deltaT))

        if len(deltaTRun)>=runMinLength:
            saveRun(deltaTRun, runsData)

        inclination=0.0
        try:
            # Fuck yeah oneliners
            inclination=float(re.findall(r'([0-9]+([\.,][0-9]*){0,1})\s*mm', dataDir)[-1][0])
        except Exception as ex:
            print "Failed inclination regex:", ex
        summary={'name': dataDir, 'inclination': inclination, 'runsData': runsData, 'startDate': firstDate, 'stopDate': date}
        summaries.append(summary)
        pickle.dump(summary, summaryFD, -1)

if __name__=='__main__':
    recurseSearch(rootDir)
    print summaries
    with open('summaries.tsv', 'w') as sFD, open('ptlist.tsv', 'w') as ptsFD:
        sFD.write('\t\t\tcount\tmean\tstddev\n')
        for s in summaries:
            sFD.write(u"""Name\t%s
StartDate\t%s
EndDate\t%s
Duration\t%d\tseconds
Inclination\t%f\tmm
Runs
"""
            % (s['name'], s['startDate'].strftime("%Y-%m-%d %H.%M.%S"), s['stopDate'].strftime("%Y-%m-%d %H.%M.%S"),(s['stopDate']-s['startDate']).total_seconds(), s['inclination']))
            longestRun=0
            longestRunMean=None
            for r in s['runsData']:
                if r['length']>longestRun:
                    longestRun=r['length']
                    longestRunMean=r['mean']
                sFD.write('\t\t\t%d\t%f\t%f\n'%(r['length'], r['mean'], r['stddev']))
            if longestRunMean:
                ptsFD.write('%f %f\n' % (s['inclination'], longestRunMean))

