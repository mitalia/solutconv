#! /bin/bash

c=0
for i in "$1"/2013*.png
do
    d=`printf %06d $c`
    ln -s "$i" "$2/$d.png"
    c=`expr $c + 1`
done
