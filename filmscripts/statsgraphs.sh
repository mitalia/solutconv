#!/bin/bash
for i in *stats.tsv
do
    out=`basename "$i" .tsv`.pdf
    gnuplot <<EOF
    set terminal pdf enhanced color font 'Helvetica,12' size 17cm,12cm
    f='$i'
    set xlabel "Tempo (s)"
    set yrange [0:50]
    set xrange [*:*]
    set output '$out'
    plot f using 1:2 with lines title 'Contrasto (a.u.)' lw 2, f using 1:3 with lines title '{/Symbol D}T (K)' lw 2
    set xrange [0:3600]
    plot f using 1:2 with lines title 'Contrasto (a.u.)' lw 2, f using 1:3 with lines title '{/Symbol D}T (K)' lw 2
EOF
done
