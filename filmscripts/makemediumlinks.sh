#!/bin/bash

c=0
for i in {60..1000}
do
    d=`printf %06d $i`
    e=`printf %06d $c`
    cp "$1/$d.png" "$2/$e.png"
    c=`expr $c + 1`
done
