from NI435x import *
pd=NI435x()
pd.Set_Scan_List('2,3')
pd.Set_Channel_Mode('2,3', NI435x.RESISTANCE)
while True:
    pd.Set_Number_Of_Scans(2)
    pd.Acquisition_StartStop(NI435x.START)
    print pd.Read(2, -1)
    pd.Acquisition_StartStop(NI435x.STOP)
pd.Close()
