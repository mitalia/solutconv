from collections import deque

""" The PIDController class represents a manually-timed PID controller"""
class PIDController:
    def __init__(self, integrationTime):
        # Controller parameters
        self.p=0
        self.i=0
        self.d=0
        # Target value
        self.target=0
        # Time step (time to assume between two measures)
        self.timeStep=1

        # Ring buffer to store measures to integrate
        self.ringBuffer=None

        self.setIntegrationTime(integrationTime)

    def setIntegrationTime(self, integrationTime):
        it=[]
        if self.ringBuffer is not None:
            # Keep the old integration samples
            it=self.ringBuffer
        self.ringBuffer=deque(it, integrationTime)

    def getIntegrationTime(self):
        return self.ringBuffer.maxlen

    def addMeasure(self, measure):
        self.ringBuffer.append(measure-self.target)

    def setTarget(self, target):
        # Clear the ringBuffer to avoid integral windup & co.
        self.ringBuffer.clear()
        self.target=target

    def getCorrection(self):
        # We need at least two samples (for the derivative part)
        if len(self.ringBuffer)<2:
            return 0
        # proportional
        ret= self.p*self.ringBuffer[-1]
        # integral
        ret+=self.i*sum(self.ringBuffer)*self.timeStep
        # derivative
        ret+=self.d*(self.ringBuffer[-2]-self.ringBuffer[-1])/float(self.timeStep)
        return ret

    def getMeanStdDev(self):
        if len(self.ringBuffer)==0:
            # we cannot compute a mean/stddev without samples
            mean=stdDev=float('NaN')
        else:
            mean=sum(self.ringBuffer)/len(self.ringBuffer)
            stdDev=(sum([(r-mean)**2 for r in self.ringBuffer])/(len(self.ringBuffer)-1))**0.5
        return (mean, stdDev)

    def invertParams(self):
        # Convenience function, used when creating an opposite-direction copy
        self.p=-self.p
        self.i=-self.i
        self.d=-self.d
