from PIL import Image
import numpy as np
import sys
from os import listdir
from os.path import isfile, join

mypath=sys.argv[1]
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath,f)) ]
for f in onlyfiles:
    if 'bg' in f:
        continue
    arr=np.asarray(Image.open(join(mypath, f))).astype(np.int16)-127
    a, u=tuple([float(t) for t in f.split(' ')[3].split('-')])
    print np.mean(np.square(arr)), a-u
