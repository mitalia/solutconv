/*= National Instruments 435X ===============================================*/
#include <visatype.h>

#if defined(__cplusplus) || defined(__cplusplus__)
extern "C" {
#endif

/*===========================================================================*/
/* Defines                                                                   */
/*===========================================================================*/
#define ni435x_export __declspec(dllexport)

/* Status Codes */
#define NI435X_SUCCESS                 0

/* Device Identification */
#define NI435X_DAQCARD_4350            71
#define NI435X_AT_4350                 72
#define NI435X_4351                    307

/* Maximum Number Of Sessions */
#define NI435X_MAX_SESSIONS            16

/* Maximum Number Of Channels */
#define NI435X_MAX_CHANNELS            16

/* Error Codes */
#define NI435X_ERROR_FAIL_ID_QUERY     -1223
#define NI435X_ERROR_PARSING           -1301
#define NI435X_WARN_NSUP_ERROR_QUERY   -1302
#define NI435X_WARN_NSUP_SELF_TEST     -1303
#define NI435X_WARN_NSUP_REV_QUERY     -1304
#define NI435X_ERROR_SETTINGS          -1305
#define NI435X_ERROR_PARAMETER2        -1306
#define NI435X_ERROR_PARAMETER3        -1307
#define NI435X_ERROR_PARAMETER4        -1308
#define NI435X_ERROR_INVALID_SESSION   -1309
#define NI435X_ERROR_MAX_SESSIONS      -1310
#define NI435X_WARN_UNKNOWN_STATUS     -1311
#define NI435X_ERROR_MEMORY            -1312
#define NI435X_ERROR_INVALID_OTC       -1313
#define NI435X_ERROR_INVALID_GND_REF   -1314
#define NI435X_ERROR_NO_CJC            -1315
#define NI435X_ERROR_NO_AUTO_ZERO      -1316
#define NI435X_ERROR_INVALID_CHAN_MODE -1317
#define NI435X_ERROR_INVALID_CHANNEL   -1318
#define NI435X_ERROR_CUSTOM_MODE       -1319
#define NI435X_WARN_AUTO_ZERO          -1320

/* Error Checking Codes */
#define NI435X_TRUE                    1
#define NI435X_FALSE                   0

/* On/Off */
#define NI435X_OFF                     0
#define NI435X_ON                      1

/*===========================================================================*/
/* Function Specific Defines                                                 */
/*===========================================================================*/

/* Set Temperature Units */
#define NI435X_DEG_F                   0
#define NI435X_DEG_C                   1
#define NI435X_DEG_K                   2
#define NI435X_DEG_R                   3

/* Set Channel Mode */
#define NI435X_VOLTAGE                 0
#define NI435X_THERM_B                 1
#define NI435X_THERM_E                 2
#define NI435X_THERM_J                 3
#define NI435X_THERM_K                 4
#define NI435X_THERM_N                 5
#define NI435X_THERM_R                 6
#define NI435X_THERM_S                 7
#define NI435X_THERM_T                 8
#define NI435X_RTD_ITS_90              9
#define NI435X_RTD_AMERICAN            10
#define NI435X_RTD_DIN_43760           11
#define NI435X_RESISTANCE              12
#define NI435X_THERM_CUSTOM            13

/* Set Powerline Frequency */
#define NI435X_50HZ                    0
#define NI435X_60HZ                    1

/* Set Reading Rate */
#define NI435X_FAST                    0
#define NI435X_SLOW                    1

/* Set CJC */
#define NI435X_AUTO                    0
#define NI435X_MANUAL                  1

/* Start/Stop Acquisition */
#define NI435X_START                   0
#define NI435X_STOP                    1

/* Current source */
#define NI435X_25uA                    0
#define NI435X_1mA                     1

/*- Auto Zero Mode */
#define NI435X_AUTO_ZERO_AT_START      1
#define NI435X_AUTO_ZERO_EACH_SCAN     2

/*===========================================================================*/
/*  Function Prototypes                                                      */
/*===========================================================================*/

/*- Added for backward compatibility ----------------------------------------*/
#define NI435X_Acuisition_StartStop NI435X_Acquisition_StartStop

ni435x_export ViStatus _VI_FUNC NI435X_Getting_Started (ViRsrc resourceName, ViChar _VI_FAR channelString[],
                        ViInt16 mode, ViReal64 lowLimit,
                        ViReal64 highLimit, ViInt32 numberOfScans,
                        ViReal64 _VI_FAR scanBuffer[]);
ni435x_export ViStatus _VI_FUNC NI435X_Getting_Started_No_Acc (ViRsrc resourceName,
                                                ViChar _VI_FAR channelString[],
                        ViInt16 mode, ViReal64 lowLimit,
                        ViReal64 highLimit, ViInt32 numberOfScans,
                        ViReal64 _VI_FAR scanBuffer[]);
ni435x_export ViStatus _VI_FUNC NI435X_Getting_Started_Multi (ViRsrc resourceName,
                        ViInt32 numberOfScans,
                        ViInt16 powerlineFreq,
                        ViInt16 readingRate,
                        ViUInt16 channelGroups,
                        ViString channelStrings[],
                        ViInt16 modes[],
                        ViReal64 lowLimits[],
                        ViReal64 highLimits[],
                        ViReal64 _VI_FAR scanBuffer[]);
ni435x_export ViStatus _VI_FUNC NI435X_Getting_Started_Digital (ViRsrc resourceName,
                        ViUInt32  lineMask,
                        ViUInt32  writePattern,
                        ViUInt32* readPattern);
ni435x_export ViStatus _VI_FUNC NI435X_init (ViRsrc resourceName, ViBoolean IDQuery,
                        ViBoolean resetDevice, ViPSession DAQsession);
ni435x_export ViStatus _VI_FUNC NI435X_Interval_Scanning (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViInt16 mode, ViReal64 lowLimit,
                        ViReal64 highLimit, ViInt32 scanInterval,
                        ViInt32 numberOfScans, ViReal64 scanBuffer[]);
ni435x_export ViStatus _VI_FUNC NI435X_Normal_Scanning(ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViInt16 mode, ViReal64 lowLimit,
                        ViReal64 highLimit, ViInt32 numberOfScans,
                        ViReal64 scanBuffer[]);
ni435x_export ViStatus _VI_FUNC NI435X_Normal_Scanning_No_Acc (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViInt16 mode, ViReal64 lowLimit,
                        ViReal64 highLimit,
                        ViInt32 numberOfScans,
                        ViReal64 _VI_FAR scanBuffer[]);
ni435x_export ViStatus _VI_FUNC NI435X_readbackOutput (ViSession DAQsession,
                        ViInt16 numPatterns,
                        ViInt32 _VI_FAR writePattern[],
                        ViInt32 _VI_FAR readPattern[]);
ni435x_export ViStatus _VI_FUNC NI435X_Configure (ViSession DAQsession,
                        ViInt32   numberOfScans,
                        ViInt16   powerlineFreq,
                        ViInt16   readingRate,
                        ViUInt16  channelGroups,
                        ViString  channelStrings[],
                        ViInt16   modes[],
                        ViReal64  lowLimits[],
                        ViReal64  highLimits[]);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Temperature_Units (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViInt16 units);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Channel_Mode (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViInt16 mode);
ni435x_export ViStatus _VI_FUNC NI435X_Configure_Custom_Mode (ViSession DAQsession,
                        ViReal64 _VI_FAR forward[],
                        ViReal64 _VI_FAR reverse[]);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Range (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViReal64  lowLimits,
                        ViReal64  highLimits);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Powerline_Frequency (ViSession DAQsession,
                        ViInt16 powerlineFrequency);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Shunt_Value (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViReal64 shuntResistance);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Auto_Zero (ViSession DAQsession, ViBoolean autoZero);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Open_TC_Detection (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViInt16 OTCDetection);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Gnd_Ref (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViInt16 groundReference);
ni435x_export ViStatus _VI_FUNC NI435X_Set_CJC (ViSession DAQsession,
                        ViChar _VI_FAR channelString[], ViInt16 mode,
                        ViReal64 value);
ni435x_export ViStatus _VI_FUNC NI435X_Set_RTD_Ice_Point (ViSession DAQsession,
                        ViChar _VI_FAR channelString[],
                        ViReal64 icePoint);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Reading_Rate (ViSession DAQsession,
                        ViInt16 readingRate);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Scan_List (ViSession DAQsession,
                        ViChar _VI_FAR channelString[]);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Number_Of_Scans (ViSession DAQsession,
                        ViInt32 numberOfScans);
ni435x_export ViStatus _VI_FUNC NI435X_Get_Number_Of_Channels (ViSession DAQsession,
                        ViUInt16 *numberOfChannels);
ni435x_export ViStatus _VI_FUNC NI435X_Check_And_Read (ViSession DAQsession,
                        ViReal64 timeout,
                        ViInt32 *scansRead,
                        ViReal64 _VI_FAR scansBuffer[]);
ni435x_export ViStatus _VI_FUNC NI435X_Read (ViSession DAQsession, ViInt32 numerOfScans,
                        ViReal64 timeout, ViReal64 _VI_FAR scansRead[]);
ni435x_export ViStatus _VI_FUNC NI435X_Acquisition_StartStop (ViSession DAQsession,
                        ViInt16 control);
ni435x_export ViStatus _VI_FUNC NI435X_Check (ViSession DAQsession, ViPInt32 scansAvailable,
                        ViPBoolean overflow);
ni435x_export ViStatus _VI_FUNC NI435X_Write_Line (ViSession DAQsession, ViInt32 lineNumber,
                        ViBoolean lineState);
ni435x_export ViStatus _VI_FUNC NI435X_Configure_Input_Port (ViSession DAQsession);
ni435x_export ViStatus _VI_FUNC NI435X_Configure_Output_Port (ViSession DAQsession);
ni435x_export ViStatus _VI_FUNC NI435X_Configure_Digital_Lines (ViSession DAQsession,
                        ViInt32 lineMask);
ni435x_export ViStatus _VI_FUNC NI435X_Read_Line (ViSession DAQsession, ViInt32 lineNumber,
                        ViPBoolean lineState);
ni435x_export ViStatus _VI_FUNC NI435X_Write_Port (ViSession DAQsession, ViInt32 pattern,
                        ViInt32 lineMask);
ni435x_export ViStatus _VI_FUNC NI435X_Read_Port (ViSession DAQsession, ViPInt32 pattern,
                        ViInt32 lineMask);
ni435x_export ViStatus _VI_FUNC NI435X_reset (ViSession DAQsession);
ni435x_export ViStatus _VI_FUNC NI435X_self_test (ViSession DAQsession, ViPInt16 selfTestResult,
                        ViChar _VI_FAR selfTestMessage[]);
ni435x_export ViStatus _VI_FUNC NI435X_revision_query (ViSession DAQsession,
                        ViChar _VI_FAR instrumentDriverRevision[],
                        ViChar _VI_FAR firmwareRevision[]);
ni435x_export ViStatus _VI_FUNC NI435X_error_query (ViSession DAQsession, ViPInt32 errorCode,
                        ViChar _VI_FAR errorMessage[]);
ni435x_export ViStatus _VI_FUNC NI435X_error_message (ViSession DAQsession, ViStatus errorCode,
                        ViChar _VI_FAR errorMessage[]);
ni435x_export ViStatus _VI_FUNC NI435X_close (ViSession DAQsession);
ni435x_export ViStatus _VI_FUNC NI435X_system_query (ViSession DAQsession, ViChar _VI_FAR nidaqVer[]);

ni435x_export ViStatus _VI_FUNC NI435X_Set_Current_Source (ViSession DAQsession,
                        ViChar _VI_FAR channelString[], ViInt16 source);
ni435x_export ViStatus _VI_FUNC NI435X_Set_Auto_Zero_Mode (ViSession DAQsession, ViInt16 autoZeroMode);

/*= End Function Prototypes =================================================*/

#if defined(__cplusplus) || defined(__cplusplus__)
}
#endif

/*****************************************************************************/
/*=== END INCLUDE FILE ======================================================*/
/*****************************************************************************/
