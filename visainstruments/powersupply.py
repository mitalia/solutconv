# -*- coding: utf-8 -*-
import visa

class PowerSupply:

    def __init__(self, visaAddress):
        self.ps=visa.instrument(visaAddress)
        self.reset()

    def reset(self):
        self.ps.write('*RST')

    def setCurr(self, curr):
        self.ps.write('CURR %0.3f' % curr)

    def getCurr(self):
        return float(self.ps.ask('CURR?'))

    def setVolt(self, volt):
        self.ps.write('VOLT %0.3f' % volt)

    def getVolt(self):
        return float(self.ps.ask('VOLT?'))

    def getMode(self):
        return self.ps.ask('FUNC:MODE?')

    def displayText(self, text):
        self.ps.write('DISP:TEXT "'+text.replace('"','')+'"')

    def setOutput(self, state):
        self.ps.write('OUTP %d' % int(bool(state)))
