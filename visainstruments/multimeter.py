# -*- coding: utf-8 -*-
import visa

class Multimeter:

    modeVoltageDC='VOLT:DC'
    modeVoltageAC='VOLT:AC'
    modeCurrentDC='CURR:DC'
    modeCurrentAC='CURR:AC'
    modeResistance='RES'
    mode4WiresResistance='FRES'
    modeFrequency='FREQ'
    modePeriod='PER'
    modeContinuity='CONT'
    modeDiode='DIOD'

    def __init__(self, visaAddress):
        self.mm=visa.instrument(visaAddress)
        self.reset()

    def reset(self):
        self.mm.write('*RST')

    def setMode(self, mode, params=''):
        self.mm.write('CONF:'+mode+' '+params)

    def singleMeasure(self, mode, params=''):
        return float(self.mm.ask('MEAS:'+mode+'? '+params))

    def read(self):
        return float(self.mm.ask('READ?'))

    def displayText(self, text):
        self.mm.write('DISP:TEXT "'+text.replace('"','')+'"')
    
    def setModeRes(self, mode, res):
        self.mm.write(mode+':RES '+res)

    def displayClear(self):
        self.mm.write('DISP:TEXT:CLEAR')
