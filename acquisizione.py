from nimodules.imaq import *
from nimodules.ni435x import *
import termistore
from PIL import Image
import numpy as np
from datetime import datetime

l=termistore.build_conversion_function(*termistore.load_XY('termistore/dati termistore.tsv'))
RtoC=lambda R: l(R)-273.15

conv = termistore
with ni435x() as niBox:
    niBox = ni435x()
    niBox.Set_Scan_List('3')
    niBox.Set_Channel_Mode('3', ni435x.RESISTANCE)
    with ImgInterface('img0') as imgInt:
        imgInt.sessionOpen()
        for i in range(1, 100):
            niBox.Set_Number_Of_Scans(1)
            niBox.Acquisition_StartStop(ni435x.START)
            temp=RtoC(niBox.Read(1, -1)[0])
            img=Image.fromarray(imgInt.snap())
            print "Temperature: ", temp, " C"
            outFile="%s - %f C.png" % (datetime.now().strftime("%Y-%m-%d %H.%M.%S"), temp)
            print "Saving to", outFile
            img.save(outFile)
